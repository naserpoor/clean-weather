package com.security.cleanweather.datasource.model

data class TemperatureData(
    val min: Double,
    val morn: Double,
    val day: Double,
    val eve: Double,
    val night: Double,
    val max: Double
)