package com.security.cleanweather.datasource.model

data class WeatherData(val id: Int, val main: String)