package com.security.cleanweather.datasource.model

data class ForecastData(val daily: List<DailyData>, val hourly: List<HourlyData>)