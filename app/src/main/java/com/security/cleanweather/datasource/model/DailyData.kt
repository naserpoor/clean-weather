package com.security.cleanweather.datasource.model

data class DailyData(val dt: Long, val temp: TemperatureData, val weather: List<WeatherData>)