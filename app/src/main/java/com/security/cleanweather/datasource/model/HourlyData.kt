package com.security.cleanweather.datasource.model

data class HourlyData(val dt: Long, val temp: Double, val weather: List<WeatherData>)