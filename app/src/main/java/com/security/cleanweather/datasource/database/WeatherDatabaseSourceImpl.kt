package com.security.cleanweather.datasource.database

import android.content.Context
import androidx.core.content.edit
import com.google.gson.GsonBuilder
import com.security.cleanweather.data.datasource.WeatherDatabaseSource
import com.security.cleanweather.domain.Entity.WeatherInfo

// this is a custom database for single row data :)
class WeatherDatabaseSourceImpl(val ctx: Context) : WeatherDatabaseSource {
    override suspend fun getWeatherInfo(): WeatherInfo {
        val prefs = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val gson = GsonBuilder().create()
        val stored = prefs.getString(PREF_KEY, PREF_DEFAULT)
        if (stored == PREF_DEFAULT) {
            return WeatherInfo.NULL
        } else {
            val data = gson.fromJson(stored, WeatherInfo::class.java)
            return data
        }
    }

    override suspend fun insertWeatherInfo(weathers: WeatherInfo) {
        val prefs = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val gson = GsonBuilder().create()
        prefs.edit(commit = true) {
            putString(PREF_KEY, gson.toJson(weathers))
        }
    }

    companion object {
        const val PREF_NAME = "WeatherDatabaseSource.pref"
        const val PREF_KEY = "Weather.key"
        const val PREF_DEFAULT = "{}"
    }
}