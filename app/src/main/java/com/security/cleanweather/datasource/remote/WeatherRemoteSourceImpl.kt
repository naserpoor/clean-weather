package com.security.cleanweather.datasource.remote

import com.security.cleanweather.data.datasource.WeatherRemoteSource
import com.security.cleanweather.domain.Entity.LocationInfo
import com.security.cleanweather.domain.Entity.Temperature
import com.security.cleanweather.domain.Entity.Weather
import com.security.cleanweather.domain.Entity.WeatherInfo

class WeatherRemoteSourceImpl(val openWeatherApi: OpenWeatherApi) : WeatherRemoteSource {
    override suspend fun getAllWeatherInfo(location: LocationInfo): WeatherInfo {
        val data = openWeatherApi.getAllWeathers(
            location.lat,
            location.lon,
            "minutely,current",
            "metric",
            "c705b34e6362699664bd38b700aa514f"
        )
        return WeatherInfo(data.hourly.map {
            Weather(
                it.dt,
                it.temp,
                Temperature.NULL,
                it.weather.getOrNull(0)?.id ?: -1
            )
        },
            data.daily.map {
                Weather(
                    it.dt,
                    it.temp.day,
                    Temperature(
                        it.temp.min,
                        it.temp.morn,
                        it.temp.day,
                        it.temp.eve,
                        it.temp.night,
                        it.temp.max
                    ),
                    it.weather.getOrNull(0)?.id ?: -1
                )
            })
    }

}