package com.security.cleanweather.datasource.remote

import com.security.cleanweather.datasource.model.ForecastData
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {
    @GET("/data/2.5/onecall")
    suspend fun getAllWeathers(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String,
        @Query("units") units: String,
        @Query("appid") appId: String
    ): ForecastData
}