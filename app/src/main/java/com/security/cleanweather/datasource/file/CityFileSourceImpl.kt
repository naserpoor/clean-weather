package com.security.cleanweather.datasource.file

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.security.cleanweather.data.datasource.CityFileSource
import com.security.cleanweather.datasource.model.CityData

class CityFileSourceImpl(val ctx: Context) : CityFileSource {

    override suspend fun getAllCities(): List<CityData> {
        val input = ctx.assets.open("IR.city.list.json")
        val gson = GsonBuilder().create()
        return gson.fromJson(input.bufferedReader(), object : TypeToken<List<CityData>>() {}.type)
    }

}