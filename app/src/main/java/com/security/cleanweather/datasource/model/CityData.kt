package com.security.cleanweather.datasource.model

data class CityData(
    val id: Long,
    val name: String,
    val state: String,
    val country: String,
    val coord: CoordinateData
)