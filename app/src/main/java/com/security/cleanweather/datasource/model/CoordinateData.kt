package com.security.cleanweather.datasource.model

data class CoordinateData(val lon: Double, val lat: Double)