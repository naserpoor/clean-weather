package com.security.cleanweather.domain.repository

import com.security.cleanweather.data.Result
import com.security.cleanweather.domain.Entity.City
import com.security.cleanweather.domain.Entity.LocationInfo

interface CitiesRepository {
    suspend fun searchCities(search: String): Result<List<City>>
    suspend fun getLocationForId(id: Long): Result<LocationInfo>
    suspend fun getCityForId(id: Long): Result<City>
}