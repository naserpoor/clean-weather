package com.security.cleanweather.domain.Entity

data class Temperature(
    val min: Double,
    val morn: Double,
    val day: Double,
    val eve: Double,
    val night: Double,
    val max: Double
) {
    companion object {
        val NULL = Temperature(-1.0, -1.0, -1.0, -1.0, -1.0, -1.0)
    }
}