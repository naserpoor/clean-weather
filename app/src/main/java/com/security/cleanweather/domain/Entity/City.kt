package com.security.cleanweather.domain.Entity

data class City(val id: Long, val name: String) {
    companion object {
        val NULL = City(-1, "")
    }
}