package com.security.cleanweather.domain.usecases

import com.security.cleanweather.domain.repository.CitiesRepository

class SearchCitiesUsecase(val citiesRepository: CitiesRepository) {
    suspend operator fun invoke(search: String) = citiesRepository.searchCities(search)
}