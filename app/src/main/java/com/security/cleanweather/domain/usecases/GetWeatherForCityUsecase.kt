package com.security.cleanweather.domain.usecases

import com.security.cleanweather.data.Result
import com.security.cleanweather.domain.Entity.WeatherInfo
import com.security.cleanweather.domain.repository.CitiesRepository
import com.security.cleanweather.domain.repository.ForecastsRepository

class GetWeatherForCityUsecase(
    val citiesRepository: CitiesRepository,
    val forecastsRepository: ForecastsRepository
) {
    suspend operator fun invoke(id: Long): Result<WeatherInfo> {
        return when (val location = citiesRepository.getLocationForId(id)) {
            is Result.Success -> {
                forecastsRepository.getWeatherInfoForLocation(location.data)
            }
            is Result.Error -> {
                Result.Error(WeatherInfo.NULL, location.error)
            }
        }
    }
}