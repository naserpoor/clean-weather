package com.security.cleanweather.domain.repository

import com.security.cleanweather.data.Result
import com.security.cleanweather.domain.Entity.LocationInfo
import com.security.cleanweather.domain.Entity.WeatherInfo

interface ForecastsRepository {
    suspend fun getWeatherInfoForLocation(location: LocationInfo): Result<WeatherInfo>
}