package com.security.cleanweather.domain.Entity

data class Weather(
    val datetime: Long,
    val avgTemp: Double,
    val temp: Temperature,
    val weatherId: Int
)