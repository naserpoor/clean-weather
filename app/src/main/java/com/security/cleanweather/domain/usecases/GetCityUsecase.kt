package com.security.cleanweather.domain.usecases

import com.security.cleanweather.domain.repository.CitiesRepository

class GetCityUsecase(val citiesRepository: CitiesRepository) {
    suspend operator fun invoke(id: Long) = citiesRepository.getCityForId(id)
}