package com.security.cleanweather.domain.Entity

data class LocationInfo(val lon: Double, val lat: Double) {
    companion object {
        val NULL = LocationInfo((-1).toDouble(), (-1).toDouble())
    }
}