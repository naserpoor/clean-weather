package com.security.cleanweather.domain.Entity

data class WeatherInfo(val hourlyForecasts: List<Weather>, val dailyForecasts: List<Weather>) {
    companion object {
        val NULL = WeatherInfo(ArrayList(), ArrayList())
    }
}