package com.security.cleanweather.utils

import android.content.Context
import com.security.cleanweather.R
import com.security.cleanweather.domain.Entity.Temperature
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object WeatherUtils {
    fun getResourceIdForWeatherCondition(timeInMillis: Long, weatherId: Int, cal: Calendar): Int {
        return when (weatherId) {
            in 200..232 -> R.drawable.ic_thunderstorm
            in 300..321 -> R.drawable.ic_rain
            in 500..504 -> R.drawable.ic_shower_raint
            511 -> R.drawable.ic_snow
            in 520..531 -> R.drawable.ic_shower_raint
            in 600..622 -> R.drawable.ic_snow
            in 701..761 -> R.drawable.ic_thunderstorm
            761, 771, 781 -> R.drawable.ic_thunderstorm
            800, in 951..957 -> when (getIndexOfTime(timeInMillis, cal)) {
                0 -> R.drawable.ic_clear_sky_dawn
                1 -> R.drawable.ic_clear_sky_morning
                2 -> R.drawable.ic_clear_sky_evening
                3 -> R.drawable.ic_clear_sky_evening
                4 -> R.drawable.ic_clear_sky_night
                else -> R.drawable.ic_clear_sky_morning
            }
            801, in 802..804 -> when (getIndexOfTime(timeInMillis, cal)) {
                0 -> R.drawable.ic_few_cloud_dawn
                1 -> R.drawable.ic_few_cloud_morning
                2 -> R.drawable.ic_few_cloud_evening
                3 -> R.drawable.ic_few_cloud_evening
                4 -> R.drawable.ic_few_clound_night
                else -> R.drawable.ic_few_cloud_morning
            }
            in 900..906 -> R.drawable.ic_thunderstorm
            in 958..962 -> R.drawable.ic_thunderstorm
            else -> {
                R.drawable.ic_thunderstorm
            }
        }
    }

    fun formatTemperature(context: Context, temperature: Double): String {
        return String.format(context.getString(R.string.format_temperature), temperature)
    }

    fun elapsedDaysSinceEpoch(utcDate: Long): Long {
        return TimeUnit.MILLISECONDS.toDays(utcDate)
    }

    fun getDayName(dateInMillis: Long): String {
        val daysFromEpochToProvidedDate = elapsedDaysSinceEpoch(dateInMillis)
        val daysFromEpochToToday = elapsedDaysSinceEpoch(System.currentTimeMillis())
        return when ((daysFromEpochToProvidedDate - daysFromEpochToToday).toInt()) {
            0 -> "Today"
            1 -> "Tomorrow"
            else -> {
                val dayFormat = SimpleDateFormat("EEEE", Locale.ENGLISH)
                dayFormat.format(dateInMillis)
            }
        }
    }

    fun getBackgroundForCurrentTime(cal: Calendar): Int {
        return when (getIndexOfTime(System.currentTimeMillis(), cal)) {
            0 -> R.drawable.background_dawn
            1 -> R.drawable.background_morning
            2 -> R.drawable.background_noon
            3 -> R.drawable.background_evening
            4 -> R.drawable.background_night
            else -> {
                R.drawable.background_morning
            }
        }
    }

    fun getTemperatureForCurrentTime(temperature: Temperature, cal: Calendar): Double {
        return when (getIndexOfTime(System.currentTimeMillis(), cal)) {
            0 -> temperature.min
            1 -> temperature.morn
            2 -> temperature.max
            3 -> temperature.eve
            4 -> temperature.night
            else -> {
                temperature.eve
            }
        }
    }

    fun getIndexOfTime(timeInMillis: Long, cal: Calendar): Int {
        cal.timeInMillis = timeInMillis
        return when (cal.get(Calendar.HOUR_OF_DAY)) {
            in 0..6 -> 0
            in 7..11 -> 1
            in 12..16 -> 2
            in 17..19 -> 3
            in 20..24 -> 4
            else -> {
                -1
            }
        }
    }
}