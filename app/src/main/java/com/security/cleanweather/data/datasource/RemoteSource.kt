package com.security.cleanweather.data.datasource

import com.security.cleanweather.domain.Entity.LocationInfo
import com.security.cleanweather.domain.Entity.WeatherInfo

interface WeatherRemoteSource {
    suspend fun getAllWeatherInfo(location: LocationInfo): WeatherInfo
}