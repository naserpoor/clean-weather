package com.security.cleanweather.data

sealed class Result<T> {
    class Success<T>(val data: T) : Result<T>()
    class Error<T>(val data: T, val error: ErrorType) :
        Result<T>()
}