package com.security.cleanweather.data.repositoryImpl

import android.util.Log
import com.security.cleanweather.data.ErrorType
import com.security.cleanweather.data.Result
import com.security.cleanweather.data.datasource.CityFileSource
import com.security.cleanweather.datasource.model.CityData
import com.security.cleanweather.domain.Entity.City
import com.security.cleanweather.domain.Entity.LocationInfo
import com.security.cleanweather.domain.repository.CitiesRepository

class CitiesRepositoryImpl(val cityFileSource: CityFileSource) : CitiesRepository {
    var cities = ArrayList<CityData>()

    private suspend fun loadData() {
        try {
            if (cities.isEmpty()) {
                val data = cityFileSource.getAllCities()
                cities.addAll(data)
            }
        } catch (e: Exception) {
            Log.d("ERROR", e.toString())
        }
    }

    override suspend fun searchCities(search: String): Result<List<City>> {
        loadData()
        return Result.Success(cities.map {
            City(it.id, it.name)
        }.filter {
            it.name.contains(search)
        })
    }

    override suspend fun getLocationForId(id: Long): Result<LocationInfo> {
        loadData()
        cities.find { it.id == id }.let {
            if (it == null)
                return Result.Error(LocationInfo.NULL, ErrorType.NotFound)
            else
                return Result.Success(
                    LocationInfo(
                        it.coord.lon,
                        it.coord.lat
                    )
                )
        }
    }

    override suspend fun getCityForId(id: Long): Result<City> {
        loadData()
        cities.find {
            it.id == id
        }.let {
            if (it == null)
                return Result.Error(City.NULL, ErrorType.NotFound)
            else
                return Result.Success(
                    City(it.id, it.name)
                )
        }
    }
}