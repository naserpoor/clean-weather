package com.security.cleanweather.data

enum class ErrorType {
    Connection,
    NotFound,
    Unknown
}