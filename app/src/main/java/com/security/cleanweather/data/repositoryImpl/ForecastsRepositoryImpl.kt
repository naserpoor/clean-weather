package com.security.cleanweather.data.repositoryImpl

import com.security.cleanweather.data.ErrorType
import com.security.cleanweather.data.Result
import com.security.cleanweather.data.datasource.WeatherDatabaseSource
import com.security.cleanweather.data.datasource.WeatherRemoteSource
import com.security.cleanweather.domain.Entity.LocationInfo
import com.security.cleanweather.domain.Entity.WeatherInfo
import com.security.cleanweather.domain.repository.ForecastsRepository

class ForecastsRepositoryImpl(
    val databaseSource: WeatherDatabaseSource,
    val remoteSource: WeatherRemoteSource
) :
    ForecastsRepository {
    var lastRequestedLocation: LocationInfo? = null
    override suspend fun getWeatherInfoForLocation(location: LocationInfo): Result<WeatherInfo> {
        if (location != lastRequestedLocation) {
            lastRequestedLocation = location
            // get new data
            try {
                val data = remoteSource.getAllWeatherInfo(location)
                databaseSource.insertWeatherInfo(data)
            } catch (e: Throwable) {
                // ignore:connection error
                lastRequestedLocation = null // try again later
                return Result.Error(databaseSource.getWeatherInfo(), ErrorType.Connection)
            }
        }
        val data = databaseSource.getWeatherInfo()
        return Result.Success(data)
    }

}