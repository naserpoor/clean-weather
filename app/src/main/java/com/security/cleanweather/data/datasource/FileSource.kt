package com.security.cleanweather.data.datasource

import com.security.cleanweather.datasource.model.CityData

interface CityFileSource {
    suspend fun getAllCities(): List<CityData>
}