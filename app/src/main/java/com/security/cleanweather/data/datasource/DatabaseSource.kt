package com.security.cleanweather.data.datasource

import com.security.cleanweather.domain.Entity.WeatherInfo

interface WeatherDatabaseSource {
    suspend fun getWeatherInfo(): WeatherInfo
    suspend fun insertWeatherInfo(weathers: WeatherInfo)
}