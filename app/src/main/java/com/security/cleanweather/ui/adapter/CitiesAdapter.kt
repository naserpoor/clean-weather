package com.security.cleanweather.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.security.cleanweather.R
import com.security.cleanweather.domain.Entity.City
import kotlinx.android.synthetic.main.item_city.view.*

class CitiesAdapter(val itemClicked: (Int) -> Unit) :
    RecyclerView.Adapter<CitiesAdapter.ViewHodler>() {
    lateinit var inflater: LayoutInflater
    var data: List<City> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
        get() = field

    class ViewHodler(itemview: View, var pos: Int) : RecyclerView.ViewHolder(itemview)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHodler {
        val vh = ViewHodler(inflater.inflate(R.layout.item_city, parent, false), 0)
        vh.itemView.setOnClickListener {
            itemClicked(vh.pos)
        }
        return vh
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHodler, position: Int) {
        holder.itemView.txt_city_name.text = data[position].name
        holder.pos = position
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        inflater = LayoutInflater.from(recyclerView.context)
    }
}