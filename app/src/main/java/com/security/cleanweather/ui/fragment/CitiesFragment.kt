package com.security.cleanweather.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.security.cleanweather.R
import com.security.cleanweather.data.Result
import com.security.cleanweather.presentation.viewmodel.CitiesViewModel
import com.security.cleanweather.ui.adapter.CitiesAdapter
import com.security.cleanweather.utils.WeatherUtils
import kotlinx.android.synthetic.main.fragment_cities.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class CitiesFragment : Fragment() {

    val viewmodel: CitiesViewModel by viewModel()
    var adapter: CitiesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cities, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = CitiesAdapter {
            val ctx = context ?: return@CitiesAdapter
            val adapter = adapter ?: return@CitiesAdapter
            ctx.getSharedPreferences(
                HomeFragment.PREF_FILE,
                Context.MODE_PRIVATE
            ).edit(commit = true) {
                putLong(HomeFragment.PREF_KEY, adapter.data[it].id)
            }
            findNavController().navigateUp()
        }
        viewmodel.getLiveData().observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Success -> {
                    adapter?.data = it.data
                }
            }
        })

        rv_cities.layoutManager = GridLayoutManager(context, 2)
        rv_cities.adapter = adapter
        edit_search.addTextChangedListener {
            viewmodel.searchCities(it?.toString() ?: "")
        }
        root_cities.setBackgroundResource(
            WeatherUtils.getBackgroundForCurrentTime(
                Calendar.getInstance(
                    TimeZone.getDefault()
                )
            )
        )
        viewmodel.searchCities("")
    }

}