package com.security.cleanweather.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.security.cleanweather.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}