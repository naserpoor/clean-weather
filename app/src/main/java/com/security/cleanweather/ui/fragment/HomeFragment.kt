package com.security.cleanweather.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.security.cleanweather.R
import com.security.cleanweather.data.ErrorType
import com.security.cleanweather.data.Result
import com.security.cleanweather.domain.Entity.City
import com.security.cleanweather.domain.Entity.Weather
import com.security.cleanweather.domain.Entity.WeatherInfo
import com.security.cleanweather.presentation.viewmodel.HomeViewModel
import com.security.cleanweather.utils.WeatherUtils
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.item_daily.view.*
import kotlinx.android.synthetic.main.item_hourly.view.*
import kotlinx.android.synthetic.main.item_hourly.view.img
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class HomeFragment : Fragment() {

    val viewmodel: HomeViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val ctx = context ?: return
        val selectedCityId =
            ctx.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE).getLong(PREF_KEY, -1)
        if (selectedCityId == -1L) {
            findNavController().navigate(R.id.action_homeFragment_to_citiesFragment)
            return
        }
        txt_city.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_citiesFragment)
        }
        list_daily.children.forEachIndexed { index, view ->
            view.setOnClickListener {
                val weather: Weather
                val cal = Calendar.getInstance(TimeZone.getDefault())
                when (val result = viewmodel.getLiveDataWeather().value) {
                    is Result.Success -> {
                        weather = result.data.dailyForecasts[index]
                        initMainView(weather, cal, ctx)
                    }
                    is Result.Error -> {
                        if (result.data == WeatherInfo.NULL) {
                            return@setOnClickListener
                        }
                        weather = result.data.dailyForecasts[index]
                        initMainView(weather, cal, ctx)
                    }
                }
            }
        }
        refreshData()
    }

    private fun consumeCityResult(result: Result<City>) {
        when (result) {
            is Result.Success -> {
                txt_city.text = result.data.name
            }
            is Result.Error -> {
            }
        }
    }

    private fun consumeWeatherResult(result: Result<WeatherInfo>, ctx: Context) {
        when (result) {
            is Result.Success -> {
                initUI(ctx, result.data)
            }
            is Result.Error -> {
                when (result.error) {
                    ErrorType.Connection -> {
                        Toast.makeText(ctx, R.string.msg_connection_error, Toast.LENGTH_SHORT)
                            .show()
                        if (result.data != WeatherInfo.NULL) {
                            initUI(ctx, result.data)
                        }
                    }
                }
            }
        }
    }

    private fun initMainView(
        weather: Weather,
        cal: Calendar,
        ctx: Context
    ) {
        txt_current_avg.text = WeatherUtils.formatTemperature(
            ctx,
            WeatherUtils.getTemperatureForCurrentTime(weather.temp, cal)
        )
        txt_current_minmax.text = WeatherUtils.formatTemperature(
            ctx,
            weather.temp.max
        ) + " / " + WeatherUtils.formatTemperature(ctx, weather.temp.min)
        img_current.setImageResource(
            WeatherUtils.getResourceIdForWeatherCondition(
                System.currentTimeMillis(),
                weather.weatherId,
                cal
            )
        )
    }

    private fun initUI(ctx: Context, data: WeatherInfo) {
        val cal = Calendar.getInstance(TimeZone.getDefault())
        list_hourly.children.forEachIndexed { index, view ->
            val weather =
                data.hourlyForecasts.getOrNull(index) ?: return@forEachIndexed
            cal.timeInMillis = weather.datetime * 1000
            view.txt_temp.text = WeatherUtils.formatTemperature(
                ctx,
                weather.avgTemp
            )
            val AM_PM = if (cal.get(Calendar.AM_PM) == Calendar.AM) "am" else "pm"
            val time = if (cal.get(Calendar.HOUR) == 0) 12 else cal.get(Calendar.HOUR).toString()
            view.txt_time.text = time.toString() + AM_PM
            view.img.setImageResource(
                WeatherUtils.getResourceIdForWeatherCondition(
                    weather.datetime * 1000,
                    weather.weatherId,
                    cal
                )
            )
        }
        list_daily.children.forEachIndexed { index, view ->
            val weather =
                data.dailyForecasts.getOrNull(index) ?: return@forEachIndexed
            view.txt_day.text = WeatherUtils.getDayName(weather.datetime * 1000)
            view.txt_temp_min.text =
                WeatherUtils.formatTemperature(ctx, weather.temp.min)
            view.txt_temp_max.text =
                WeatherUtils.formatTemperature(ctx, weather.temp.max)
            view.img.setImageResource(
                WeatherUtils.getResourceIdForWeatherCondition(
                    System.currentTimeMillis(),
                    weather.weatherId,
                    cal
                )
            )
            if (index == 0) {
                initMainView(weather, cal, ctx)
            }
        }
    }

    private fun refreshData() {
        val ctx = context ?: return
        val selectedCityId =
            ctx.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE).getLong(PREF_KEY, -1)
        viewmodel.startFetchData(selectedCityId).observe(viewLifecycleOwner, Observer {
            viewmodel.getLiveDataCity().value?.let {
                consumeCityResult(it)
            }
            viewmodel.getLiveDataWeather().value?.let {
                consumeWeatherResult(it, ctx)
            }
            progress_home.visibility = View.GONE
            root_home.visibility = View.VISIBLE
            val cal = Calendar.getInstance(TimeZone.getDefault())
            root_home.setBackgroundResource(WeatherUtils.getBackgroundForCurrentTime(cal))
        })
        progress_home.visibility = View.VISIBLE
        root_home.visibility = View.GONE
    }

    companion object {
        const val PREF_FILE = "HomeFragment.pref"
        const val PREF_KEY = "SelectedCityId.key"
    }

}