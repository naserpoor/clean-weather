package com.security.cleanweather

import android.app.Application
import com.security.cleanweather.di.dataModule
import com.security.cleanweather.di.datasourceModule
import com.security.cleanweather.di.domainModule
import com.security.cleanweather.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MainApplication)
            modules(dataModule, datasourceModule, domainModule, presentationModule)
        }
    }
}