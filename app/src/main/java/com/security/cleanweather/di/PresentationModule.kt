package com.security.cleanweather.di

import com.security.cleanweather.presentation.viewmodel.CitiesViewModel
import com.security.cleanweather.presentation.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel { CitiesViewModel(get()) }
    viewModel { HomeViewModel(get(), get()) }
}