package com.security.cleanweather.di

import com.security.cleanweather.data.repositoryImpl.CitiesRepositoryImpl
import com.security.cleanweather.data.repositoryImpl.ForecastsRepositoryImpl
import com.security.cleanweather.domain.repository.CitiesRepository
import com.security.cleanweather.domain.repository.ForecastsRepository
import org.koin.dsl.module

val dataModule = module {
    single<CitiesRepository> { CitiesRepositoryImpl(get()) }
    single<ForecastsRepository> { ForecastsRepositoryImpl(get(), get()) }
}