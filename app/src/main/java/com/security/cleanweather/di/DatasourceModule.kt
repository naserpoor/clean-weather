package com.security.cleanweather.di

import android.content.Context
import com.google.gson.Gson
import com.security.cleanweather.R
import com.security.cleanweather.data.datasource.CityFileSource
import com.security.cleanweather.data.datasource.WeatherDatabaseSource
import com.security.cleanweather.data.datasource.WeatherRemoteSource
import com.security.cleanweather.datasource.database.WeatherDatabaseSourceImpl
import com.security.cleanweather.datasource.file.CityFileSourceImpl
import com.security.cleanweather.datasource.remote.OpenWeatherApi
import com.security.cleanweather.datasource.remote.WeatherRemoteSourceImpl
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val datasourceModule = module {
    single<WeatherDatabaseSource> { WeatherDatabaseSourceImpl(get()) }
    single<CityFileSource> { CityFileSourceImpl(get()) }
    single<WeatherRemoteSource> { WeatherRemoteSourceImpl(get()) }
    single { retrofit(get()) }
    single { restApi(get()) }
}

private fun retrofit(
    context: Context
): Retrofit {
    return Retrofit.Builder()
        .baseUrl(context.getString(R.string.openweather_uri))
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .build()
}

private fun restApi(retrofit: Retrofit): OpenWeatherApi =
    retrofit.create(OpenWeatherApi::class.java)
