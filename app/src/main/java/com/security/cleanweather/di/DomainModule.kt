package com.security.cleanweather.di

import com.security.cleanweather.domain.usecases.GetCityUsecase
import com.security.cleanweather.domain.usecases.GetWeatherForCityUsecase
import com.security.cleanweather.domain.usecases.SearchCitiesUsecase
import org.koin.dsl.module

val domainModule = module {
    single { SearchCitiesUsecase(get()) }
    single { GetWeatherForCityUsecase(get(), get()) }
    single { GetCityUsecase(get()) }
}