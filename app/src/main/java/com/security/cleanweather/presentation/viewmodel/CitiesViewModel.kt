package com.security.cleanweather.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.security.cleanweather.data.Result
import com.security.cleanweather.domain.Entity.City
import com.security.cleanweather.domain.usecases.SearchCitiesUsecase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class CitiesViewModel(val usecase: SearchCitiesUsecase) : ViewModel() {
    private var job: Job? = null
    private val liveData = MutableLiveData<Result<List<City>>>()
    fun searchCities(search: String) {
        job?.cancel()
        job = null
        job = viewModelScope.launch(Dispatchers.IO) {
            liveData.postValue(usecase(search))
        }
    }

    fun getLiveData(): LiveData<Result<List<City>>> = liveData
}