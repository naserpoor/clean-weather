package com.security.cleanweather.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.security.cleanweather.data.Result
import com.security.cleanweather.domain.Entity.City
import com.security.cleanweather.domain.Entity.WeatherInfo
import com.security.cleanweather.domain.usecases.GetCityUsecase
import com.security.cleanweather.domain.usecases.GetWeatherForCityUsecase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class HomeViewModel(
    private val getWeather: GetWeatherForCityUsecase,
    private val getCity: GetCityUsecase
) : ViewModel() {
    private var job: Job? = null
    private val liveDataWeather = MutableLiveData<Result<WeatherInfo>>()
    private val liveDataCity = MutableLiveData<Result<City>>()
    fun startFetchData(id: Long): LiveData<Boolean> {
        val result = MutableLiveData<Boolean>()
        job?.cancel()
        job = null
        job = viewModelScope.launch(Dispatchers.IO) {
            val data1 = getCity(id)
            liveDataCity.postValue(data1)
            val data2 = getWeather(id)
            liveDataWeather.postValue(data2)
            result.postValue(true)
        }
        return result
    }

    fun getLiveDataWeather(): LiveData<Result<WeatherInfo>> = liveDataWeather
    fun getLiveDataCity(): LiveData<Result<City>> = liveDataCity
}